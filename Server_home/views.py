from django.shortcuts import redirect, render
from Server_home.models import StatusLight,SelectControl,EveryControl
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

import socket 
from django.core import serializers
import time

##############


def home_page(request):
  #  new_user = User.objects.create_user('AdamFrk','AdamFrk@hotmail.com', '123456')
  #  new_user.is_staff = True
  #  new_user.save()
   # new_user = User.objects.create_user('cattylover','cattylover@hotmail.com', 'cat1234')
   # new_user.is_staff = True
   # new_user.save()
   # new_user = User.objects.create_user('1935_Frank','1935_Frank@hotmail.com', 'gardenO')
   # new_user.is_staff = True
   # new_user.save()
   # new_user = User.objects.create_user('S_sea','S_sea@hotmail.com', 'beautysea')
   # new_user.is_staff = True
    #new_user.save()
   # login
    if(request.method == 'POST' and request.POST.get('send_login', '') == 'send'):
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
        else:
            alarm = 'error. please try again.'
            return render(request, 'registration/login.html', {'alarm': alarm})
   
    # logout
    if(request.method == 'POST' and request.POST.get(
      'submit_logout_page', '') == 'go_logout'):
        auth.logout(request)
    return render(request, 'home.html')
########################################

def home(request):
    return render(request, 'home_back.html')

def Select_floor(request):
    return render(request, 'Select_pageT.html')


def select(request):
    return render(request, 'select.html')

def select_floor1(request):
    return render(request, 'select_floor1.html')

def select_floor2(request):
    return render(request, 'select_floor2.html')

def selectevent_floor1(request, Light_id):
    return render(request, 'select_event.html', {'Light_id': Light_id})



def Sigleevent_floor1(request, Light_id):
    year= int(time.strftime("%Y"))
    month = int(time.strftime("%m"))
    day = int(time.strftime("%d"))
    TimeNow= float(time.strftime("%H.%M"))
    TimeNowS= time.strftime("%H . %M")
    dd7=[]
    dm7=[]
    dy7=[]
    dd7.append(day)
    dm7.append(month)      
    dy7.append(year) 
    #print(day)
    for i in range(1,7):
      if(month==1 or month==3 or month==5 or month==7 or month==8 or month==9 or month==10 or month==12):
         if day+i<31:
            dd7.append(day+i)
            dm7.append(month)      
            dy7.append(year)         
         else :
            dd7.append(day-31+i)
            if month==12 :
              dm7.append(1)
              dy7.append(year+1)
            else :
              dm7.append(month+1)
              dy7.append(year)  
      elif(month==4 or month==6 or month==9 or month==11):
         if day+i<30:
            dd7.append(day+i)
            dm7.append(month)       
            dy7.append(year)          
         else :
            dd7.append(day-30+i)
            dm7.append(month+1) 
            dy7.append(year)  
      elif(month==2):
         if day+i<28:
            dd7.append(day+i)
            dm7.append(month)      
            dy7.append(year)          
         else :
            dd7.append(day-28+i)
            dm7.append(month+1)
            dy7.append(year)  
    String7Day=[]    
    for m in range(len(dd7)): 
         String7Day.append(str(dd7[m])+"/" +str(dm7[m])+"/"+str(dy7[m]))
    print(String7Day)
    if (request.method == 'POST' and request.POST.get(
      'Update_send_Light', '') == 'submit_send_update'):
         if(request.POST['Time_text']!=''):
            Want=float(request.POST['Time_text'])
            print(Want)
            if ((Want>=TimeNow and int(request.POST['Date'])-1 ==0 ) or int(request.POST['Date'])-1 !=0):
                  SelectControl.objects.create(
                             light=Light_id,
                             status=request.POST['Status'],
                             date=String7Day[int(request.POST['Date'])-1],
                             time=request.POST['Time_text'],
                                 )          
         return redirect('/select' )    
    return render(request, 'Sigleevent_floor1.html',  {'Light_id': Light_id,'d1':String7Day[0],'d2':String7Day[1],'d3':String7Day[2],'d4':String7Day[3],'d5':String7Day[4],'d6':String7Day[5],'d7':String7Day[6] ,'TimeNow':TimeNowS})

##################################################


def tableSigle(request):  
    Events = SelectControl.objects.all()
    if(request.method == 'POST' and request.POST.get(
      'delete', '') == 'delete'):
        id_data = request.POST['id_delete']
        SelectControl.objects.get(pk=id_data).delete()
        return redirect('/tableSigle')
    return render(request, 'TableSigle.html', {'Events':Events})
##################################################################

def Repeatedevent_floor1(request, Light_id):  
    if (request.method == 'POST' and request.POST.get(
      'Update_send_Light', '') == 'submit_send_update'):
         if(request.POST['Every_text']!=''):
            Every=int(request.POST['Every_text'])
            print(Every)
            if (Every>=15):
                  EveryControl.objects.create(
                             light=Light_id,
                             state=request.POST['state'],
                             time=Every,
                                 )          
         return redirect('/select' )    
    return render(request, 'Repeatedevent_floor1.html', {'Light_id': Light_id})
#######################################################################



def tableRepeated(request):  
    Events = EveryControl.objects.all()
    if(request.method == 'POST' and request.POST.get(
      'delete', '') == 'delete'):
        id_data = request.POST['id_delete']
        EveryControl.objects.get(pk=id_data).delete()
        return redirect('/tableRepeated')
    return render(request, 'TableRepeated.html', {'Events':Events})



def select_floor2(request):
    return render(request, 'select_floor2.html')
##############################################
def sendLight1():
    address = ("192.168.1.60", 8000) #set address Arduino
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.settimeout(1) 
    ## socket.AF_INET, # Internet
    ##SOCK_DGRAM # UDP
    Light_ = StatusLight.objects.get(id=1)
    send_data = str(Light_.status1+Light_.status2+Light_.status3+Light_.status4) #define data
    sock.sendto(send_data.encode(), address) #send data
    print ("send_data1 ", send_data) 

#############################################
def home_floor1(request):
 if StatusLight.objects.count() == 0:
       StatusLight.objects.create(status1='1',status2='1',status3='1',status4='1',)
 Light_ = StatusLight.objects.get(id=1)
 if(request.method == 'POST' and request.POST.get(
      'light1_floor1', '') == 'light1_t'):
        if(Light_.status1 =='1') : 
          Light_.status1='0'
        else:
          Light_.status1='1'
 if(request.method == 'POST' and request.POST.get(
      'light2_floor1', '') == 'light2_t'):
        if(Light_.status2 =='1') : 
          Light_.status2='0'
        else:
          Light_.status2='1'
 if(request.method == 'POST' and request.POST.get(
      'light3_floor1', '') == 'light3_t'):
        if(Light_.status3 =='1') : 
          Light_.status3='0'
        else:
          Light_.status3='1'
 if(request.method == 'POST' and request.POST.get(
      'light4_floor1', '') == 'light4_t'):
        if(Light_.status4 =='1') : 
          Light_.status4='0'
        else:
          Light_.status4='1'
 Light_.save()
 #sendLight1()
 UDP_IP="169.254.150.162"
 address = ("169.254.150.163", 8080) #set address Arduino
 UDP_Port=8080
 sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
 sock.bind((UDP_IP,UDP_Port))
 sock.settimeout(1) 
    ## socket.AF_INET, # Internet
    ##SOCK_DGRAM # UDP
 Light_ = StatusLight.objects.get(id=1) 
 send_data = str(Light_.status1+Light_.status2+Light_.status3+Light_.status4) #define data
 sock.sendto(send_data.encode(), address) #send data
 print ("send_data1 ", send_data)
 return render(request, 'floor_1.html', {'Light_': Light_})
#############################################


def home_floor2(request):
 if StatusLight.objects.count() == 0:
       StatusLight.objects.create(status1='1',status2='1',status3='1',status4='1',)
       StatusLight.objects.create(status1='1',status2='1',status3='1',status4='1',)
 if StatusLight.objects.count() == 1:
       StatusLight.objects.create(status1='1',status2='1',status3='1',status4='1',)
 Light2_ = StatusLight.objects.get(id=2)
 if(request.method == 'POST' and request.POST.get(
      'light1_floor1', '') == 'light1_t'):
        if(Light2_.status1 =='1') : 
          Light2_.status1='0'
        else:
          Light2_.status1='1'
 if(request.method == 'POST' and request.POST.get(
      'light2_floor1', '') == 'light2_t'):
        if(Light2_.status2 =='1') : 
          Light2_.status2='0'
        else:
          Light2_.status2='1'
 if(request.method == 'POST' and request.POST.get(
      'light3_floor1', '') == 'light3_t'):
        if(Light2_.status3 =='1') : 
          Light2_.status3='0'
        else:
          Light2_.status3='1'
 if(request.method == 'POST' and request.POST.get(
      'light4_floor1', '') == 'light4_t'):
        if(Light2_.status4 =='1') : 
          Light2_.status4='0'
        else:
          Light2_.status4='1'
 Light2_.save()
 UDP_IP="169.254.150.162"
 address = ("169.254.150.163", 8080) #set address Arduino
 UDP_Port=8080
 sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
 sock.bind((UDP_IP,UDP_Port))
 sock.settimeout(1) 
    ## socket.AF_INET, # Internet
    ##SOCK_DGRAM # UDP
 Light_ = StatusLight.objects.get(id=2) 
 send_data = str(Light_.status1+Light_.status2+Light_.status3+Light_.status4) #define data
 sock.sendto(send_data.encode(), address) #send data
 print ("send_data2 ", send_data)
 return render(request, 'floor_2.html', {'Light2_': Light2_})
#############################################


def login_page(request):
    if(request.method == 'POST' and request.POST.get(
      'submit_login_page', '') == 'go_login'):
        return redirect('/login_page')
    return render(request, 'registration/login.html')



    # lookup user by id and send them a message
if 1 :
  print("Yell")


