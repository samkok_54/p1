# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Server_home', '0002_auto_20151213_1219'),
    ]

    operations = [
        migrations.RenameField(
            model_name='statuslight',
            old_name='status',
            new_name='status2',
        ),
        migrations.AddField(
            model_name='statuslight',
            name='status1',
            field=models.TextField(default='1'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='statuslight',
            name='status3',
            field=models.TextField(default='1'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='statuslight',
            name='status4',
            field=models.TextField(default='1'),
            preserve_default=True,
        ),
    ]
