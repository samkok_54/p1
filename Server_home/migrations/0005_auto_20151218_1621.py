# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Server_home', '0004_selectcontrol'),
    ]

    operations = [
        migrations.CreateModel(
            name='EveryControl',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('light', models.TextField(default='1')),
                ('state', models.TextField(default='1')),
                ('time', models.IntegerField(default=15)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='selectcontrol',
            name='time',
            field=models.TextField(default='18.50'),
            preserve_default=True,
        ),
    ]
