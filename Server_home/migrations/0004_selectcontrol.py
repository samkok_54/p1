# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Server_home', '0003_auto_20151213_1226'),
    ]

    operations = [
        migrations.CreateModel(
            name='SelectControl',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('light', models.TextField(default='1')),
                ('status', models.TextField(default='1')),
                ('date', models.TextField(default='1')),
                ('time', models.TextField(default='1')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
