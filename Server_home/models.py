from django.db import models
from django.contrib.auth.models import User

class StatusLight(models.Model):
    status1 = models.TextField(default='1')
    status2 = models.TextField(default='1')
    status3 = models.TextField(default='1')
    status4 = models.TextField(default='1')

class SelectControl(models.Model):
    light = models.TextField(default='1')
    status = models.TextField(default='1')
    date = models.TextField(default='1')
    time = models.TextField(default='18.50')

class EveryControl(models.Model):
    light = models.TextField(default='1')
    state = models.TextField(default='1')
    time = models.IntegerField(default=15)
