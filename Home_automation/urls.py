from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'Server_home.views.home_page', name='home'),
    url(r'^home_floor1$', 'Server_home.views.home_floor1',name='home_floor1'),
    url(r'^home_floor2$', 'Server_home.views.home_floor2',name='home_floor2'),
    url(r'^login_page$', 'Server_home.views.login_page', name='Login'), 
    url(r'^home$', 'Server_home.views.home', name='home_back'),
    url(r'^Select_floor$', 'Server_home.views.Select_floor', name='Select_pageT'),
    url(r'^select$', 'Server_home.views.select', name='select'),
    url(r'^select_floor1$', 'Server_home.views.select_floor1', name='select_floor1'),
    url(r'^select_floor2$', 'Server_home.views.select_floor2', name='select_floor2'),
    url(r'^select/(\d+)/event/floor1/$', 'Server_home.views.selectevent_floor1', name='selectevent_floor1'), #####sam edit
    url(r'^select/(\d+)/Sigleevent/floor1/$', 'Server_home.views.Sigleevent_floor1', name='Sigleevent_floor1'), #####sam edit
    url(r'^select/(\d+)/Repeatedevent/floor1/$', 'Server_home.views.Repeatedevent_floor1', name='Repeatedevent_floor1'), #####sam edit
    url(r'^tableSigle$', 'Server_home.views.tableSigle', name='tableSigle'),
    url(r'^tableRepeated$', 'Server_home.views.tableRepeated', name='tableRepeated'),
]

